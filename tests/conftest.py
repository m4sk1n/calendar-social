# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Helper functions and fixtures for testing
"""

from contextlib import contextmanager

import pytest

from helpers import configure_app


@pytest.fixture
def client():
    """Fixture that provides a Flask test client
    """

    from calsocial import app
    from calsocial.models import db

    configure_app()
    client = app.test_client()

    with app.app_context():
        db.create_all()

    yield client

    with app.app_context():
        db.drop_all()


@pytest.fixture
def database():
    """Fixture to provide all database tables in an active application context
    """

    from calsocial import app
    from calsocial.models import db

    configure_app()

    with app.app_context():
        db.create_all()

        yield db

        db.drop_all()
