# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""General tests for Calendar.social
"""

from flask import current_app

import pytest

def test_index_no_login(client):
    """Test the main page without logging in
    """

    page = client.get('/')
    assert b'Peek inside' in page.data


def test_instance_adin_unset(database):
    """Test the instance admin feature if the admin is not set
    """

    with pytest.warns(UserWarning, match=r'Instance admin is not set correctly \(value is None\)'):
        assert current_app.instance_admin is None


def test_instance_admin_bad_value(database):
    """Test the instance admin feature if the value is invalid
    """

    from calsocial.models import AppState

    AppState['instance_admin'] = 'value'

    with pytest.warns(UserWarning, match=r'Instance admin is not set correctly \(value is value\)'):
        assert current_app.instance_admin is None


def test_instance_admin_doesnot_exist(database):
    """Test the instance admin feature if the admin user does not exist
    """

    from calsocial.models import AppState

    AppState['instance_admin'] = '0'

    with pytest.warns(UserWarning, match=r'Instance admin is not set correctly \(value is 0\)'):
        assert current_app.instance_admin is None


def test_instance_admin(database):
    """Test the instance admin feature if the admin user does not exist
    """

    from calsocial.models import db, AppState, User

    user = User(username='admin')
    db.session.add(user)
    db.session.commit()

    AppState['instance_admin'] = user.id

    assert current_app.instance_admin == user
