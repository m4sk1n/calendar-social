# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Profile related tests for Calendar.social
"""

import calsocial
from calsocial.models import db, Notification, NotificationAction, Profile, User, UserFollow

from helpers import login


def test_profile_follow(database):
    """Test the Profile.follow() method
    """

    follower_user = User(username='follower',
                         email='follower@example.com',
                         password='passworder',
                         active=True)
    followed_user = User(username='followed', email='followed@example.com')
    follower = Profile(display_name='Follower', user=follower_user)
    followed = Profile(display_name='Followed', user=followed_user)
    db.session.add_all([follower, followed])
    db.session.commit()

    user_follow = followed.follow(follower=follower)
    db.session.commit()

    # The new follower record should have the fields set correctly
    assert user_follow.followed == followed
    assert user_follow.follower == follower

    # There should be a notification about the follow
    notification = Notification.query.one()

    assert notification.actor == follower
    assert notification.item == followed
    assert notification.action == NotificationAction.follow

    assert follower in followed.follower_list
    assert followed in follower.followed_list


def test_follow_ui(client):
    """Test following on the web interface
    """

    with calsocial.app.app_context():
        follower_user = User(username='follower',
                             email='follower@example.com',
                             password='passworder',
                             active=True)
        followed_user = User(username='followed', email='followed@example.com')
        follower = Profile(display_name='Follower', user=follower_user)
        followed = Profile(display_name='Followed', user=followed_user)
        db.session.add_all([follower, followed])
        db.session.commit()

    login(client, 'follower', 'passworder')

    page = client.get('/profile/@followed/follow')
    assert page.status_code == 302
    assert page.location == 'http://localhost/profile/%40followed'

    with calsocial.app.app_context():
        db.session.add_all([follower, followed])
        follow = UserFollow.query.one()
        assert follow.follower == follower
        assert follow.followed == followed
        assert follow.accepted_at is not None


def test_locked_profile(database):
    """Test following a locked profile
    """

    follower_user = User(username='follower',
                         email='follower@example.com',
                         password='passworder',
                         active=True)
    followed_user = User(username='followed', email='followed@example.com')
    follower = Profile(display_name='Follower', user=follower_user)
    followed = Profile(display_name='Followed', user=followed_user, locked=True)
    db.session.add_all([follower, followed])
    db.session.commit()

    user_follow = followed.follow(follower=follower)
    db.session.commit()

    # The new follower record should have the fields set correctly
    assert user_follow.followed == followed
    assert user_follow.follower == follower
    assert not user_follow.accepted_at

    # There should be a notification about the follow
    notification = Notification.query.one()

    assert notification.actor == follower
    assert notification.item == followed
    assert notification.action == NotificationAction.follow

    assert follower not in followed.follower_list
    assert followed not in follower.followed_list
