from datetime import datetime, date

from pytz import utc

from calsocial.calendar_system.gregorian import GregorianCalendar


def test_day_list():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.days[0].date() == date(2018, 1, 1)
    assert calendar.days[-1].date() == date(2018, 2, 4)

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.days[0].date() == date(2018, 11, 26)
    assert calendar.days[-1].date() == date(2019, 1, 6)


def test_prev_year():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_year == datetime(2017, 1, 1, 0, 0, 0)

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_year == datetime(2017, 12, 1, 0, 0, 0)


def test_prev_year_year():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_year_year == 2017

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_year_year == 2017


def test_prev_month():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_month == datetime(2017, 12, 1, 0, 0, 0)

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_month == datetime(2018, 11, 1, 0, 0, 0)


def test_prev_month_name():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_month_name == 'December'

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.prev_month_name == 'November'


def test_next_year():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.next_year == datetime(2019, 1, 1, 0, 0, 0)

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.next_year == datetime(2019, 12, 1, 0, 0, 0)


def test_next_year_year():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.next_year_year == 2019

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.next_year_year == 2019


def test_next_month():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.next_month == datetime(2018, 2, 1, 0, 0, 0)

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.next_month == datetime(2019, 1, 1, 0, 0, 0)


def test_next_month_name():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.next_month_name == 'February'

    calendar = GregorianCalendar(utc.localize(datetime(2018, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.next_month_name == 'January'


def test_has_today():
    calendar = GregorianCalendar(utc.localize(datetime(1990, 12, 1, 0, 0, 0)).timestamp())
    assert calendar.has_today is False

    calendar = GregorianCalendar(utc.localize(datetime.utcnow()).timestamp())
    assert calendar.has_today is True


def test_current_month():
    calendar = GregorianCalendar(utc.localize(datetime(2018, 1, 1, 0, 0, 0)).timestamp())
    assert calendar.month == 'January, 2018'
