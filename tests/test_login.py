# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""General tests for Calendar.social
"""

import calsocial
from calsocial.models import db, User

from helpers import login


def test_login_invalid_user(client):
    """Test logging in with a non-existing user
    """

    page = login(client, 'username', 'password')
    assert b'Specified user does not exist' in page.data


def test_login_bad_password(client):
    """Test logging in with a bad password
    """

    with calsocial.app.app_context():
        user = User(username='test', email='test@example.com', password='password')
        db.session.add(user)
        db.session.commit()

    page = login(client, 'test', password='else')
    assert b'Invalid password' in page.data


def test_login_email(client):
    """Test logging in with the email address instead of the username
    """

    with calsocial.app.app_context():
        user = User(username='test', email='test@example.com', password='password', active=True)
        db.session.add(user)
        db.session.commit()

    page = login(client, 'test@example.com', password='password')
    assert b'Logged in as ' in page.data


def test_login_first_steps(client):
    """Test logging in with a new user

    They must be redirected to the first login page
    """

    with calsocial.app.app_context():
        user = User(username='test', email='test@example.com', password='password', active=True)
        db.session.add(user)
        db.session.commit()

    page = login(client, 'test', password='password', no_redirect=True)
    # First, we must be redirected to the main page
    assert page.location == 'http://localhost/'

    page = client.get('/')
    assert page.location == 'http://localhost/accounts/first-steps'
