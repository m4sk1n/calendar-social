# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def test_app_state_set(database):
    from calsocial.models import db, AppState

    AppState['test'] = 'value'

    state = AppState.query \
                    .filter(AppState.env == 'testing') \
                    .filter(AppState.key == 'test') \
                    .one()

    assert state.value == 'value'


def test_app_state_get(database):
    from calsocial.models import db, AppState

    state = AppState(env='testing', key='test', value='value')
    db.session.add(state)
    db.session.commit()

    assert AppState['test'] == 'value'


def test_app_state_setdefault(database):
    from calsocial.models import AppState

    AppState['test'] = 'value'
    AppState.setdefault('test', 'new value')

    assert AppState['test'] == 'value'

    AppState.setdefault('other_test', 'value')
    assert AppState['other_test'] == 'value'
