# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""General tests for Calendar.social
"""

import calsocial
from calsocial.models import db, User

from helpers import alter_config


def test_register_page(client):
    """Test the registration page
    """

    page = client.get('/accounts/register')
    assert b'Register</button>' in page.data

def test_register_post_empty(client):
    """Test sending empty registration data
    """

    page = client.post('/accounts/register', data={})
    assert b'This field is required' in page.data

def test_register_invalid_email(client):
    """Test sending an invalid email address
    """

    page = client.post('/accounts/register', data={
        'username': 'test',
        'email': 'test',
        'password': 'password',
        'password_retype': 'password',
    })
    assert b'Invalid email address' in page.data

def test_register_password_mismatch(client):
    """Test sending different password for registration
    """

    page = client.post('/accounts/register', data={
        'username': 'test',
        'email': 'test@example.com',
        'password': 'password',
        'password_retype': 'something',
    })
    assert b'The two passwords must match' in page.data

def test_register(client):
    """Test user registration
    """

    page = client.post('/accounts/register', data={
        'username': 'test',
        'email': 'test@example.com',
        'password': 'password',
        'password_retype': 'password',
    })
    assert page.status_code == 302
    assert page.location == 'http://localhost/'

    with calsocial.app.app_context():
        user = User.query.one()

    assert user.username == 'test'
    assert user.email == 'test@example.com'

def test_register_existing_username(client):
    """Test registering an existing username
    """

    with calsocial.app.app_context():
        user = User(username='test', email='test@example.com')
        db.session.add(user)
        db.session.commit()

    page = client.post('/accounts/register', data={
        'username': 'test',
        'email': 'test2@example.com',
        'password': 'password',
        'password_retype': 'password',
    })
    assert b'This username is not available' in page.data

def test_register_existing_email(client):
    """Test registering an existing email address
    """

    with calsocial.app.app_context():
        user = User(username='test', email='test@example.com')
        db.session.add(user)
        db.session.commit()

    page = client.post('/accounts/register', data={
        'username': 'tester',
        'email': 'test@example.com',
        'password': 'password',
        'password_retype': 'password',
    })
    assert b'This email address can not be used' in page.data


def test_registration_disabled(client):
    with alter_config(calsocial.app, REGISTRATION_ENABLED=False):
        page = client.get('/accounts/register')
        assert b'Registration is disabled' in page.data
