Calendar.social
===============

Keep your calendar ond organise events in the Fediverse.

How to contribute
-----------------

First, see our Code of Conduct.  That’s probably the most important part.

We are more than interested in

- Feature ideas.  The project is in its early phase, so every idea is welcome (even if it doesn’t
  get into the first release.)
- Bug reports.  Even though the project is in the early phase, we don’t want bugs to sneak in to
  stay there forever.  Report everything you find.
- Documentation.  Because well, obviously.
- Fixes, features, actually implemented.
  - Find your favourite issue/feature request
  - Fork and clone the repository
  - Create a branch for your work
  - Code!
  - Push to your branch
  - Open a Pull Request
  - Go through the review process with us
  - Enjoy your work getting into the main tree

FAQ
---

Why doing a federated calendar when you can send around vCard files via email?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Because this project is about much more than a plain calendar.  It’s also an event organiser,
where you will be able to collaborate with others making events happen.

Why is it not on GitHub/GitLab/any other popular hosting service?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Because running my stuff is one of my hobby.  I run my own Matrix homeserver, my own Mastodon
instance, why would my Git repositories be different?  It might change in the future, though; I’m
not an enemy of said services and if this project turns out to be successful I might move it away
from here.
